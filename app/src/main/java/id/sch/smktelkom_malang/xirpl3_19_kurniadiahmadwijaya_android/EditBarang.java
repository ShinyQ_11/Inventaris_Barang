package id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import es.dmoral.toasty.Toasty;
import id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android.Sqlite.DataHelper;

public class EditBarang extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    EditText tv1,tv2,tv3,tv4,tv5,tv6;
    Button btnEdit;
    private int mYearIni, mMonthIni, mDayIni, sYearIni, sMonthIni, sDayIni;
    static final int DATE_ID = 0;
    Calendar C = Calendar.getInstance();

    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            mYearIni = year;
            mMonthIni = month;
            mDayIni = dayOfMonth;
            tv3.setText( mDayIni+"-"+(mMonthIni + 1)+"-"+ mYearIni+ " ");
        }
    };

    protected Dialog onCreateDialog(int id){
        switch (id){
            case DATE_ID:
                return new DatePickerDialog(this, mDateSetListener,sYearIni,sMonthIni,sDayIni);
        }
        return null;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_barang);
        dbHelper = new DataHelper(this);
        tv1 = findViewById(R.id.editText1);
        tv2 = findViewById(R.id.editText2);
        tv3 = findViewById(R.id.editText3);
        tv4 = findViewById(R.id.editText4);
        tv5 = findViewById(R.id.editText5);
        tv6 = findViewById(R.id.editText6);
        btnEdit = findViewById(R.id.btnEdit);

        sMonthIni = C.get(Calendar.MONTH);
        sDayIni = C.get(Calendar.DAY_OF_MONTH);
        sYearIni = C.get(Calendar.YEAR);

        tv3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_ID);
            }
        });
        btnEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase db = dbHelper.getWritableDatabase();
                db.execSQL("update barang set nama_brg='" +
                        tv1.getText().toString() + "', spek_brg='" +
                        tv2.getText().toString() + "', tgl='" +
                        tv3.getText().toString() + "', nama_ruang='" +
                        tv4.getText().toString() + "', harga_satuan='" +
                        tv5.getText().toString() + "', satuan_brg='" +
                        tv6.getText().toString() + "' where nama_brg='" +
                        getIntent().getStringExtra("nama_brg") + "'");
                Toasty.success(EditBarang.this, "Data Berhasil Di Edit", Toast.LENGTH_SHORT, true).show();
                MainActivity.main.RefreshList();
                finish();
            }
        });

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM barang WHERE nama_brg = '" +
                getIntent().getStringExtra("nama_brg") + "'", null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            tv1.setText(cursor.getString(1).toString());
            tv2.setText(cursor.getString(2).toString());
            tv3.setText(cursor.getString(3).toString());
            tv4.setText(cursor.getString(4).toString());
            tv5.setText(cursor.getString(7).toString());
            tv6.setText(cursor.getString(8).toString());
        }
    }
}


