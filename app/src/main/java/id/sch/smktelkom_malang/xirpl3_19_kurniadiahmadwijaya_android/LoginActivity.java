package id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import es.dmoral.toasty.Toasty;
import id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android.Koneksi.VolleySingleton;
import id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android.Model.UserModel;
import id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android.Session.SessionPreference;

public class LoginActivity extends AppCompatActivity {
    private final String url = "http://10.1.5.10:8080/users";
    EditText etUsername, etPassword;
    Button btnLogin;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        progressDialog =new ProgressDialog(this);

        if (SessionPreference.getInstance(this).isLoggedIn()){
            finish();
            startActivity(new Intent(this, MainActivity.class));
        }

        etUsername = findViewById(R.id.etUsername);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btn_login);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etUsername.length() == 0 || etPassword.length() == 0){
                    Toasty.warning(LoginActivity.this, "Data Harus Diisi Lengkap !", Toast.LENGTH_SHORT, true).show();
                }else {
                    userLogin();
                }
            }
        });

    }

    private void userLogin() {
        progressDialog.setMessage("Mohon Tunggu ...");
        progressDialog.show();
        final String username = etUsername.getText().toString();
        final String password = etPassword.getText().toString();

        StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try{
                    JSONArray jsonArray = new JSONArray(response);
                    for (int x = 0 ; x < jsonArray.length() ; x++){
                        JSONObject obj = jsonArray.getJSONObject(x);
                        String user = obj.getString("UserName");
                        String pass = obj.getString("Password");
                        if (username.equals(user) && password.equals(pass)){
                            UserModel userModel = new UserModel(
                                    obj.getInt("IDUser"),
                                    obj.getString("Name"),
                                    obj.getString("UserName"),
                                    obj.getString("Password"),
                                    obj.getString("Branch")
                            );
                            progressDialog.dismiss();
                            Toasty.success(LoginActivity.this, "Login Berhasil !", Toast.LENGTH_SHORT, true).show();
                            SessionPreference.getInstance(getApplicationContext()).userLogin(userModel);
                            startActivity(new Intent(getApplicationContext(),MainActivity.class));
                            finish();
                        }
                    }
                } catch (JSONException e) {
                    Toast.makeText(getApplicationContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(),error.getMessage(),Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }
}

