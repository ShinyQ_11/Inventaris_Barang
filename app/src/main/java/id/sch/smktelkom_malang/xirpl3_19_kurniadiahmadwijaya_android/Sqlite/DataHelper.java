package id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android.Sqlite;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "data.db";
    private static final int DATABASE_VERSION = 1;

    // Constructor dari class DataHelper
    public DataHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Method oCreate berfungsi untuk membuat database
    @Override
    public void onCreate(SQLiteDatabase db) {
        String sql = "create table barang(id_inventaris varchar(50) primary key, id_aset integer, nama_brg text null, spek_brg text null, tgl text null, nama_ruang text null, kategori text null, jenis text null, harga_satuan integer null, satuan_brg text null );";
        Log.d("Data", "onCreate: " + sql);
        db.execSQL(sql);

    }

    @Override
    public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
    }
}
