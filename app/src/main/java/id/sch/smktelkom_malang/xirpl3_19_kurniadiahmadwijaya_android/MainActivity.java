package id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.ajts.androidmads.library.SQLiteToExcel;

import java.io.File;

import es.dmoral.toasty.Toasty;
import id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android.Model.UserModel;
import id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android.Session.SessionPreference;
import id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android.Sqlite.DataHelper;

public class MainActivity extends AppCompatActivity {
    public static MainActivity main;
    TextView tvNama, tvCabang;

    protected Cursor cursor;
    String[] daftar;
    ListView lvBarang;
    Menu menu;
    DataHelper dbcenter;
    private android.support.v7.widget.Toolbar toolbar;

    void RefreshList() {
        SQLiteDatabase db = dbcenter.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM barang", null);
        daftar = new String[cursor.getCount()];
        cursor.moveToFirst();
        for (int cc = 0; cc < cursor.getCount(); cc++) {
            cursor.moveToPosition(cc);
            daftar[cc] = cursor.getString(1).toString();
        }
        lvBarang = findViewById(R.id.lvBarang);
        lvBarang.setAdapter(new ArrayAdapter(this, android.R.layout.simple_list_item_1, daftar));
        lvBarang.setSelected(true);
        lvBarang.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView arg0, View arg1, int
                    arg2, long arg3) {
                final String selection = daftar[arg2];

                                        Intent i = new
                                                Intent(getApplicationContext(), LihatBarang.class);
                                        i.putExtra("nama_brg", selection);
                                        startActivity(i);

//                                        Intent in = new
//                                                Intent(getApplicationContext(), EditBarang.class);
//                                        in.putExtra("nama_brg", selection);
//                                        startActivity(in);

//                                        SQLiteDatabase db =
//                                                dbcenter.getWritableDatabase();
//                                        db.execSQL("delete from barang where nama_brg = '" + selection + "'");
//                                        RefreshList();
//                                        Toasty.success(MainActivity.this, "Sukses Hapus", Toast.LENGTH_SHORT, true).show();
                                }
                            });
        ((ArrayAdapter)lvBarang.getAdapter()).notifyDataSetInvalidated();
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (!SessionPreference.getInstance(this).isLoggedIn()){
            finish();
            startActivity(new Intent(this, LoginActivity.class));
        }

        tvNama = findViewById(R.id.tvNama);
        tvCabang = findViewById(R.id.tvCabang);
        toolbar = findViewById(R.id.tb_main);
        setSupportActionBar(toolbar);


        UserModel user = SessionPreference.getInstance(this).getUser();
        tvNama.setText("Selamat Datang " +user.getNama());
        tvCabang.setText("(" +user.getCabang()+")");
        FloatingActionButton fabTambah = findViewById(R.id.fabTambah);
        fabTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(),TambahBarang.class));
            }
        });


        main = this;
        dbcenter = new DataHelper(this);
        RefreshList();
    }

    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.Export:
                if (ContextCompat.checkSelfPermission(MainActivity.this, android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {

                    if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        String directory_path = Environment.getExternalStorageDirectory().getPath() + "/Backup/";
                        File file = new File(directory_path);
                        if (!file.exists()) {
                            file.mkdirs();
                        }

                        SQLiteToExcel sqliteToExcel = new SQLiteToExcel(getApplicationContext(), DataHelper.DATABASE_NAME, directory_path);
                        sqliteToExcel.exportAllTables("barang.xls", new SQLiteToExcel.ExportListener() {
                            @Override
                            public void onStart() {
                                Toasty.warning(MainActivity.this, "Sedang Memproses", Toast.LENGTH_SHORT, true).show();
                            }

                            @Override
                            public void onCompleted(String filePath) {
                                Toasty.success(MainActivity.this, "Data Berhasil Di Export", Toast.LENGTH_SHORT, true).show();
                            }

                            @Override
                            public void onError(Exception e) {
                                Toasty.error(MainActivity.this, "Gagal Mengexport", Toast.LENGTH_SHORT, true).show();
                            }
                        });
                    }

                    else{
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                    }
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 9);
                }

                break;

            case R.id.logout:
                finish();
                SessionPreference.getInstance(getApplicationContext()).logout();
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

}

