package id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.IOException;

import es.dmoral.toasty.Toasty;
import id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android.Sqlite.DataHelper;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;
import jxl.write.Label;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;

public class LihatBarang extends AppCompatActivity {
    protected Cursor cursor;
    DataHelper dbHelper;
    TextView tv1, tv2, tv3,tv4,tv5,tv6,tv7,tv8,tv9;
    public DataHelper db;
    Button Edit, Hapus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lihat_barang);
        dbHelper = new DataHelper(this);
        //TODO : Buat Upload Image Ke Sqlite
        tv1 = findViewById(R.id.textView1);
        tv2 = findViewById(R.id.textView3);
        tv3 = findViewById(R.id.textView4);
        tv4 = findViewById(R.id.textView5);
        tv5 = findViewById(R.id.textView6);
        tv6 = findViewById(R.id.textView7);
        tv7 = findViewById(R.id.textView8);
        tv8 = findViewById(R.id.textView9);
        tv9 = findViewById(R.id.textView10);

        Edit = findViewById(R.id.btn_edit);
        Hapus = findViewById(R.id.btn_hapus);
        db = new DataHelper(this);


        Edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new
                        Intent(getApplicationContext(), EditBarang.class);
                in.putExtra("nama_brg", getIntent().getStringExtra("nama_brg"));
                startActivity(in);
            }
        });

        Hapus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SQLiteDatabase helper = db.getWritableDatabase();
                helper.execSQL("delete from barang where nama_brg = '" + getIntent().getStringExtra("nama_brg") + "'");
                Toasty.success(LihatBarang.this, "Sukses Hapus", Toast.LENGTH_SHORT, true).show();
                Intent intent = new Intent(LihatBarang.this, MainActivity.class);
                startActivity(intent);
            }
        });

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        cursor = db.rawQuery("SELECT * FROM barang WHERE nama_brg = '" +
                getIntent().getStringExtra("nama_brg") + "'", null);
        cursor.moveToFirst();
        if (cursor.getCount() > 0) {
            cursor.moveToPosition(0);
            tv1.setText("No Aset : "+cursor.getString(0).toString());
            tv2.setText(cursor.getString(1).toString());
            tv3.setText(cursor.getString(2).toString());
            tv4.setText("Tanggal Pengadaan : "+cursor.getString(3).toString());
            tv5.setText(cursor.getString(4).toString());
            tv6.setText(cursor.getString(5).toString());
            tv7.setText(cursor.getString(6).toString());
            tv8.setText(cursor.getString(7).toString());
            tv9.setText(cursor.getString(8).toString());
        }
    }
}
