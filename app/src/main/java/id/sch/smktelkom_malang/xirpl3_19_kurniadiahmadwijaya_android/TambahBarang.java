package id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Random;

import es.dmoral.toasty.Toasty;
import id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android.Koneksi.VolleySingleton;
import id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android.Model.UserModel;
import id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android.Session.SessionPreference;
import id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android.Sqlite.DataHelper;

public class TambahBarang extends AppCompatActivity {
    private final String url_cat = "http://10.1.5.10:8080/categories";
    private final String url_type = "http://10.1.5.10:8080/categorytypes";
    protected Cursor cursor;
    DataHelper dbHelper;
    Button btnTambah;
    private EditText etNoAset, etNamaBrg, etSpekBrg,etTglBrg,etRoomBrg, etHarga,etSatuanBrg;
    private Spinner spJenis, spKategori;
    ArrayList<String> category = new ArrayList<String>();
    ArrayList<String> elektronik = new ArrayList<String>();
    ArrayList<String> nonElektronik = new ArrayList<String>();
    ArrayAdapter<String> dataAdapter2;
    ArrayAdapter<String> dataAdapter1;
    private int mYearIni, mMonthIni, mDayIni, sYearIni, sMonthIni, sDayIni;
    static final int DATE_ID = 0;
    Calendar C = Calendar.getInstance();
    TextView idT, idC, Random;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tambah_barang);
        dbHelper = new DataHelper(this);
        btnTambah = findViewById(R.id.btnTambah);
        etNoAset = findViewById(R.id.etNoAset);
        etNamaBrg = findViewById(R.id.etNamaBrg);
        etSpekBrg = findViewById(R.id.etSpekBrg);
        etTglBrg = findViewById(R.id.etTglBrg);
        etRoomBrg = findViewById(R.id.etRoomBrg);
        etHarga = findViewById(R.id.etHarga);
        etSatuanBrg = findViewById(R.id.etSatuanBrg);
        Random = findViewById(R.id.random);


        idT = findViewById(R.id.idt);
        idC = findViewById(R.id.idc);

        sMonthIni = C.get(Calendar.MONTH);
        sDayIni = C.get(Calendar.DAY_OF_MONTH);
        sYearIni = C.get(Calendar.YEAR);

        UserModel user = SessionPreference.getInstance(this).getUser();

        etTglBrg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog(DATE_ID);
            }
        });

        loadCategory();
        loadType();

        spKategori = findViewById(R.id.spKategori);
        spJenis = findViewById(R.id.spJenis);

        dataAdapter1 = new ArrayAdapter<String>(TambahBarang.this, android.R.layout.simple_spinner_dropdown_item, category);
        dataAdapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spKategori.setAdapter(dataAdapter1);

        spKategori.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String selectedItem = parent.getItemAtPosition(position).toString();
                switch (selectedItem) {
                    case "elektronik":
                        setadapter(0);

                        break;
                    case "non elektronik":
                        setadapter(1);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

            private void setadapter(int a) {
                switch (a) {
                    case 0:
                        dataAdapter2 = new ArrayAdapter<String>(TambahBarang.this, android.R.layout.simple_spinner_dropdown_item, elektronik);
                        break;
                    case 1:
                        dataAdapter2 = new ArrayAdapter<String>(TambahBarang.this, android.R.layout.simple_spinner_dropdown_item, nonElektronik);
                        break;
                }
                dataAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spJenis.setAdapter(dataAdapter2);
            }

        });


        RandomGenerator randomGenerator = new RandomGenerator();
        for(int i = 0; i < 2; i++){
           Random.setText(randomGenerator.generateActivationCode(2));
        }
        String r = Random.getText().toString();
        String Idt = idT.getText().toString();
        String Idc = idC.getText().toString();
        String Cabang = user.getCabang().toString();

        final String Inventaris = Cabang + 2018 + "A" + 2 + "/" + r;

        btnTambah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etNoAset.length() == 0 || etNamaBrg.length() == 0 || etSpekBrg.length() == 0 || etTglBrg.length() == 0 || etRoomBrg.length() == 0 || spKategori.getSelectedItem().toString() == "Pilih Kategori" || spJenis.getSelectedItem().toString() == "" || etHarga.length()==0 || etSatuanBrg.length() == 0){
                    Toasty.success(TambahBarang.this, "Sukses Menambahkan", Toast.LENGTH_SHORT, true).show();
                }else {
                    SQLiteDatabase db = dbHelper.getWritableDatabase();
                    db.execSQL("insert into barang(id_inventaris, id_aset, nama_brg, spek_brg,tgl,nama_ruang,kategori,jenis,harga_satuan,satuan_brg) values('" +
                            Inventaris + "','" +
                            etNoAset.getText().toString() + "','" +
                            etNamaBrg.getText().toString() + "','" +
                            etSpekBrg.getText().toString() + "','" +
                            etTglBrg.getText().toString() + "','" +
                            etRoomBrg.getText().toString() + "','" +
                            spKategori.getSelectedItem().toString() + "','" +
                            spJenis.getSelectedItem().toString() + "','" +
                            etHarga.getText().toString() + "','" +
                            etSatuanBrg.getText().toString() + "')");
                    Toast.makeText(getApplicationContext(),"Data Berhasil Di Inputkan",Toast.LENGTH_SHORT).show();
                    MainActivity.main.RefreshList();
                    finish();
                }
            }
        });
    }
    private DatePickerDialog.OnDateSetListener mDateSetListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            mYearIni = year;
            mMonthIni = month;
            mDayIni = dayOfMonth;
            etTglBrg.setText( mDayIni+"-"+(mMonthIni + 1)+"-"+ mYearIni+ " ");
        }
    };

    protected Dialog onCreateDialog(int id){
        switch (id){
            case DATE_ID:
                return new DatePickerDialog(this, mDateSetListener,sYearIni,sMonthIni,sDayIni);
        }
        return null;
    }

    private void loadCategory() {
        category.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_cat, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    category.add("Pilih Kategori");
                    JSONArray jsonArray = new JSONArray(response);
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        category.add(obj.getString("Category"));
                        idC.setText(obj.getString("IDCategory"));
                    }
                    Log.d("Data", category.toString());
                    spKategori.setAdapter(dataAdapter1);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    private void loadType() {
        elektronik.clear();
        nonElektronik.clear();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url_type, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONArray jsonArray = new JSONArray(response);
                    elektronik.add("Pilih Elektronik");
                    nonElektronik.add("Pilih Non-Elektronik");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject obj = jsonArray.getJSONObject(i);
                        if (obj.getString("IDCategory").equals("A")) {
                            elektronik.add(obj.getString("Type"));
                            idT.setText(obj.getString("IDType"));
                        } else if (obj.getString("IDCategory").equals("B")) {
                            nonElektronik.add(obj.getString("Type"));
                            idT.setText(obj.getString("IDType"));
                        }
                    }
                    Log.d("Data", elektronik.toString());
                    Log.d("Data", nonElektronik.toString());
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        VolleySingleton.getInstance(this).addToRequestQueue(stringRequest);
    }

    public static class RandomGenerator{
        private  static  Random random = new Random();
        public static String generateActivationCode(int length){
            String code = new String("");
            for(int i = 0; i < length; i++){
                code += (char) (random.nextInt(10) + '0');
            }
            return code;
        }
    }
}

