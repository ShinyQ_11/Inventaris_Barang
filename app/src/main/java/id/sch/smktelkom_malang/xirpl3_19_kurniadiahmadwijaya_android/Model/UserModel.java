package id.sch.smktelkom_malang.xirpl3_19_kurniadiahmadwijaya_android.Model;

public class UserModel {
    int id;
    String nama, username, password, cabang;

    public UserModel(int id, String nama, String username, String password, String cabang) {
        this.id = id;
        this.nama = nama;
        this.username = username;
        this.password = password;
        this.cabang = cabang;
    }

    public int getId() {
        return id;
    }

    public String getNama() {
        return nama;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getCabang() {
        return cabang;
    }
}